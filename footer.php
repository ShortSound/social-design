<?php
/**
 *  The template for displaying Footer.
 *
 * @package lawyeria-lite
 */
?>
<footer id="footer">
    <div class="wrapper cf">
        <div class="footer-margin-left cf">
<!--            --><?php
//            if (is_active_sidebar('footer-sidebar')) {
//                dynamic_sidebar('footer-sidebar');
//            } else {
//                echo '<div class="footer-box">' . __('The sidebar is not active.', 'lawyeria-lite') . '</div>';
//            }
//            ?>
            <?php
            if (is_home()) { ?>
                <div id="text-3" class="footer-box widget_text contacts-widget"><div class="footer-box-title">Контакты</div>			<div class="textwidget">Полтава
                        <br>
                        Улица Lorem Ipsum, nr. 2
                        <br>
                        Телефон:1-292-330-4780
                        <br>
                        E-mail: office@mail.com</div>
                </div>
                <div id="text-4" class="footer-box widget_text form-widget">
                    <div class="footer-box-title">Свяжитесь с нами</div>
                    <div class="textwidget">
                        <div role="form" class="wpcf7" id="wpcf7-f1700-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response"></div>
                            <form action="#" method="post" class="wpcf7-form" novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="1700">
                                    <input type="hidden" name="_wpcf7_version" value="4.4.2">
                                    <input type="hidden" name="_wpcf7_locale" value="RU_US">
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1700-o1">
                                    <input type="hidden" name="_wpnonce" value="11e6b99318">
                                </div>
                                <p>
                                    <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name"
                                                                                           value="" size="40"
                                                                                           class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required full-input-text"
                                                                                           aria-required="true"
                                                                                           aria-invalid="false"
                                                                                           placeholder="Ваше имя"></span>
                                </p>

                                <p>
                                    <span class="wpcf7-form-control-wrap your-emai"><input type="email" name="your-emai"
                                                                                           value="" size="40"
                                                                                           class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email small-input-text"
                                                                                           aria-required="true"
                                                                                           aria-invalid="false"
                                                                                           placeholder="Ваш E-mail"></span>
                                </p>

                                <p>
                                    <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                              name="your-phone-number"
                                                                                              value="" size="40"
                                                                                              class="wpcf7-form-control wpcf7-text small-input-text input-no-margin"
                                                                                              aria-invalid="false"
                                                                                              placeholder="Ваш номер"></span>
                                </p>

                                <p>
                                    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message"
                                                                                                 cols="40" rows="10"
                                                                                                 class="wpcf7-form-control wpcf7-textarea"
                                                                                                 aria-invalid="false"
                                                                                                 placeholder="Введите Ваш комментарий"></textarea></span>
                                </p>

                                <p><input type="submit" value="Отправить" class="wpcf7-form-control wpcf7-submit"><img
                                        class="ajax-loader"
                                        src="https://demo.themeisle.com/lawyeria-lite/wp-content/plugins/contact-form-7/images/ajax-loader.gif"
                                        alt="Отправка ..." style="visibility: hidden;"></p>

                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="text-5" class="footer-box widget_text vk-widget">
                    <div class="footer-box-title">Мы Вконтакте</div>
                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?139"></script>

                    <!-- VK Widget -->
                    <div id="vk_groups"></div>
                    <script type="text/javascript">
                        VK.Widgets.Group("vk_groups", {mode: 3, width: "250", color3: '394753'}, 54970889);
                    </script>
            <?php }
            ?>
        </div>
        <!--/div .footer-margin-left .cf-->
    </div>
    <!--/div .wrapper .cf-->
</footer><!--/footer #footer-->
<?php wp_footer(); ?>
</body>
</html>
